<?php
/**
 * @file
 * Views file for search_api_solr_date_sort.
 */

/**
 * Implementation of hook_views_plugins().
 */
function search_api_solr_date_sort_views_plugins() {

  $plugins = array();

  $plugins['row']['search_api_solr_date_sort'] = array(
    'title' => t('Render Entity with Date Field'),
    'help' => t('Render an entity with a single date field'),
    'handler' => 'search_api_solr_date_sort_views_plugin_row_entity_view',
    'parent' => 'entity_views_plugin_row_entity_view',
    'uses fields' => FALSE,
    'uses options' => TRUE,
    'type' => 'normal',
  );

  $plugins['query']['search_api_solr_date_sort_views_query'] = array(
    'title' => t('Search API Solr Date Sort Views Query'),
    'help' => t('Query will be generated and ran through using a custom query to allow sorting by date.'),
    'handler' => 'SearchApiSolrDateSortViewsQuery',
    'parent' => 'search_api_views_query',
  );

  return $plugins;
}
